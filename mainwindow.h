/*-----------------------------------------------------*/
/*     Project APC created by QtCreator 2020-09-27     */
/*              Author Hamed taherie                   */
/*                Hamed.t@live.com                     */
/*                   alphabit.ir                       */
/*-----------------------------------------------------*/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
#include <QMainWindow>
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
class MainWindow : public QMainWindow
{
    Q_OBJECT
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
private slots:
    void onDataTimerTimeout();
    void onActionTimerTimeout();
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    void on_pBExit_clicked();

    void on_pBSetPos_clicked();

    void on_pBTest_clicked();

    void on_sBInterval_valueChanged(int arg1);

    void on_sBMin_valueChanged(int arg1);

    void on_sBSec_valueChanged(int arg1);

    void on_sBMsec_valueChanged(int arg1);

    void on_pBStop_clicked();

    void on_sBHour_valueChanged(int arg1);

    void on_pBRun_clicked();

private:
    Ui::MainWindow *ui;

    QTimer *dataTimer;
    QTimer *actionTimer;

    QPoint mousePos;


    void checkSpTime();
};
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
#endif // MAINWINDOW_H
