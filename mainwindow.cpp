/*-----------------------------------------------------*/
/*     Project APC created by QtCreator 2020-09-27     */
/*              Author Hamed taherie                   */
/*                Hamed.t@live.com                     */
/*                   alphabit.ir                       */
/*-----------------------------------------------------*/
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTimer>
#include <QDebug>
#include "windows.h"
#include <QtTest/QtTest>
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
QTime currentTime;
QTime clickTime;
bool running = false;
int clickCount = 0;
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
MainWindow::MainWindow(QWidget *parent): QMainWindow(parent),ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint |Qt::WindowStaysOnTopHint);
    setWindowFlags(windowFlags() &~ Qt::WindowContextHelpButtonHint);
    //+++++++++++++++++++++++++++++++++++++++++++++
    dataTimer = new QTimer(this);
    dataTimer->setTimerType(Qt::PreciseTimer);
    connect(dataTimer, SIGNAL(timeout()),this, SLOT(onDataTimerTimeout()));
    dataTimer->start(1);
    //+++++++++++++++++++++++++++++++++++++++++++++
    actionTimer = new QTimer(this);
    actionTimer->setTimerType(Qt::PreciseTimer);
    connect(actionTimer, SIGNAL(timeout()),this, SLOT(onActionTimerTimeout()));
    actionTimer->setInterval(ui->sBInterval->value());
    //+++++++++++++++++++++++++++++++++++++++++++++
    currentTime = QDateTime::currentDateTime().time();
    clickTime   = QDateTime::currentDateTime().time();

    ui->sBHour->setValue(currentTime.hour());
    ui->sBMin->setValue(currentTime.minute());
    ui->sBSec->setValue(currentTime.second());
    ui->sBMsec->setValue(0);
    //+++++++++++++++++++++++++++++++++++++++++++++
    ui->sBX->setValue(0);
    ui->sBY->setValue(0);
    //+++++++++++++++++++++++++++++++++++++++++++++
    ui->labelAuth->setText("Programmed by Hamed Taheri (<a href=\"http://hamed-taheri.com/\">Hamed-Taheri.com!</a>)");
    ui->labelAuth->setTextFormat(Qt::RichText);
    ui->labelAuth->setTextInteractionFlags(Qt::TextBrowserInteraction);
    ui->labelAuth->setOpenExternalLinks(true);
    //+++++++++++++++++++++++++++++++++++++++++++++
    ui->sBMin->setFocus();
    running = false;
}
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
MainWindow::~MainWindow()
{
    dataTimer->stop();
    delete dataTimer;

    actionTimer->stop();
    delete actionTimer;

    delete ui;
}
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
void MainWindow::checkSpTime()
{

    if (ui->sBX->value() <= 0)
        ui->sBX->setStyleSheet("background-color: rgba(255, 50, 5, 225);");
    else
        ui->sBX->setStyleSheet("background-color: rgba(255, 255, 255, 255);");

    if (ui->sBY->value() <= 0)
        ui->sBY->setStyleSheet("background-color: rgba(255, 50, 5, 225);");
    else
        ui->sBY->setStyleSheet("background-color: rgba(255, 255, 255, 255);");


    if (clickTime <= currentTime)
    {
        ui->sBHour->setStyleSheet("background-color: rgba(255, 50, 5, 225);");
        ui->sBMin->setStyleSheet("background-color: rgba(255, 50, 5, 225);");
        ui->sBSec->setStyleSheet("background-color: rgba(255, 50, 5, 225);");
        ui->sBMsec->setStyleSheet("background-color: rgba(255, 50, 5, 225);");
        ui->pBRun->setEnabled(false);
    } else
    {
        ui->sBHour->setStyleSheet("background-color: rgba(255, 255, 255, 255);");
        ui->sBMin->setStyleSheet("background-color: rgba(255, 255, 255, 255);");
        ui->sBSec->setStyleSheet("background-color: rgba(255, 255, 255, 255);");
        ui->sBMsec->setStyleSheet("background-color: rgba(255, 255, 255, 255);");
        ui->pBRun->setEnabled(true);
    }
}
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
void MainWindow::onDataTimerTimeout()
{
    currentTime = QDateTime::currentDateTime().time();

    if (running)
    {
        if (currentTime >= clickTime)
        {
            cursor().setPos(ui->sBX->value(), ui->sBY->value());
            mouse_event(MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, 1, 1, 0, 0);

            ui->statusbar->showMessage("Automatically clicked at :" + currentTime.toString("hh:mm:ss:zzz"),20000);

            if (ui->sBCount->value() > 1)
            {
                dataTimer->stop();
                actionTimer->start(ui->sBInterval->value());
            } else
            {
                running = false;

                ui->gBCount->setEnabled(true);
                ui->gBPoint->setEnabled(true);
                ui->gBIntervak->setEnabled(true);
                ui->gBClickTime->setEnabled(true);
                ui->pBRun->setEnabled(clickTime > currentTime);
                ui->pBTest->setEnabled(true);

                checkSpTime();
            }
        }
    } else
        if (ui->pBSetPos->isChecked())
        {
            mousePos = cursor().pos();
            ui->sBX->setValue(mousePos.x());
            ui->sBY->setValue(mousePos.y());
        }
}
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
void MainWindow::onActionTimerTimeout()
{
    cursor().setPos(ui->sBX->value(), ui->sBY->value());
    mouse_event(MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, 1, 1, 0, 0);

    if (++clickCount >= (ui->sBCount->value() - 1))
    {
        running = false;

        ui->gBCount->setEnabled(true);
        ui->gBPoint->setEnabled(true);
        ui->gBIntervak->setEnabled(true);
        ui->gBClickTime->setEnabled(true);
        ui->pBRun->setEnabled(clickTime > currentTime);
        ui->pBTest->setEnabled(true);

        checkSpTime();

        clickCount = 0;

        actionTimer->stop();
        dataTimer->start(1);
    }
}
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
void MainWindow::on_pBExit_clicked()
{
    close();
}
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
void MainWindow::on_pBSetPos_clicked()
{
    if (ui->pBSetPos->isChecked())
    {
        ui->sBX->setStyleSheet("background-color: rgba(255, 150, 5, 225);");
        ui->sBY->setStyleSheet("background-color: rgba(255, 150, 5, 225);");
    } else
    {
        ui->sBX->setStyleSheet("background-color: rgba(255, 255, 255, 255);");
        ui->sBY->setStyleSheet("background-color: rgba(255, 255, 255, 255);");

        checkSpTime();
    }
}
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
void MainWindow::on_pBTest_clicked()
{
    currentTime = QDateTime::currentDateTime().time();

    cursor().setPos(ui->sBX->value(), ui->sBY->value());
    mouse_event(MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, 1, 1, 0, 0);

    ui->statusbar->showMessage("Manually clicked at :" + currentTime.toString("hh:mm:ss:zzz"),20000);

    checkSpTime();

    if (ui->sBCount->value() <= 1)
        return;

    actionTimer->start(ui->sBInterval->value());

    ui->gBCount->setEnabled(false);
    ui->gBPoint->setEnabled(false);
    ui->gBIntervak->setEnabled(false);
    ui->gBClickTime->setEnabled(false);
    ui->pBRun->setEnabled(false);
    ui->pBTest->setEnabled(false);
}
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
void MainWindow::on_sBInterval_valueChanged(int arg1)
{
    actionTimer->setInterval(arg1);
}
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
void MainWindow::on_sBHour_valueChanged(int arg1)
{
    clickTime.setHMS(arg1,ui->sBMin->value(),ui->sBSec->value(),ui->sBMsec->value());

    checkSpTime();
}
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
void MainWindow::on_sBMin_valueChanged(int arg1)
{
    clickTime.setHMS(ui->sBHour->value(),arg1,ui->sBSec->value(),ui->sBMsec->value());

    checkSpTime();
}
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
void MainWindow::on_sBSec_valueChanged(int arg1)
{
    clickTime.setHMS(ui->sBHour->value(),ui->sBMin->value(),arg1,ui->sBMsec->value());

    checkSpTime();
}
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
void MainWindow::on_sBMsec_valueChanged(int arg1)
{
    clickTime.setHMS(ui->sBHour->value(),ui->sBMin->value(),ui->sBSec->value(),arg1);

    checkSpTime();
}
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
void MainWindow::on_pBStop_clicked()
{
    ui->gBCount->setEnabled(true);
    ui->gBPoint->setEnabled(true);
    ui->gBIntervak->setEnabled(true);
    ui->gBClickTime->setEnabled(true);

    ui->pBRun->setEnabled(clickTime > currentTime);
    ui->pBTest->setEnabled(true);

    actionTimer->stop();

    ui->statusbar->showMessage("Stopped!",10000);

    checkSpTime();

    running = false;
}
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
void MainWindow::on_pBRun_clicked()
{
    actionTimer->setInterval(ui->sBInterval->value());

    clickTime.setHMS(ui->sBHour->value(),ui->sBMin->value(),ui->sBSec->value(),ui->sBMsec->value());

    checkSpTime();

    if (clickTime <= currentTime) return;

    ui->gBCount->setEnabled(false);
    ui->gBPoint->setEnabled(false);
    ui->gBIntervak->setEnabled(false);
    ui->gBClickTime->setEnabled(false);

    ui->pBRun->setEnabled(false);
    ui->pBTest->setEnabled(false);

    ui->statusbar->showMessage("Running...");
    running = true;
    actionTimer->stop();
    clickCount = 0;
    dataTimer->start(1);
}
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
