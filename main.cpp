/*-----------------------------------------------------*/
/*     Project APC created by QtCreator 2020-09-27     */
/*              Author Hamed taherie                   */
/*                Hamed.t@live.com                     */
/*                   alphabit.ir                       */
/*-----------------------------------------------------*/
#include "mainwindow.h"

#include <QApplication>
#include <QThread>
#include "windows.h"
#include <QStyleFactory>
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
    SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
    QThread::currentThread()->setPriority(QThread::TimeCriticalPriority);

    QApplication a(argc, argv);
    a.setStyle(QStyleFactory::create("Fusion"));
    MainWindow w;
    w.show();
    w.setFixedSize(540,250);
    return a.exec();
}
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
